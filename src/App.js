import { useState } from 'react';
import './App.scss';
import Login from './components/Login';
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import useToken from './hooks/useToken';
import Dashboard from './components/Dashboard';
import Noticeboard from './components/Noticeboard/Noticeboard';
import Nav from './components/Nav';
import Header from './components/Header';

function App() {
  

  const {token, setToken} = useToken()

  if(!token ) { 

    return (<BrowserRouter>
       
       <div className="wrapper">
          <Routes>     
              <Route path="login" element={<Login setToken={setToken} />} />
              
          </Routes>
      </div>
  </BrowserRouter>
    );
  }

  return (   
    <BrowserRouter>
      <Header /> 
      <Nav />
       <div className="wrapper">
          <Routes>     
              <Route path="dashboard" element={<Dashboard />} />
              <Route path="dasnoticeboards/:id" element={<Noticeboard></Noticeboard>}></Route>
          </Routes>
      </div>
  </BrowserRouter>
  );
}

export default App;
