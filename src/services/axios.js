import axios from "axios";
import {getToken} from "../hooks/useToken";

const axios2 = axios.create({ 
    baseURL: "http://noticeboards.test"
})

axios2.interceptors.request.use(
    (config) => {
      const token = getToken();
      const auth = token ? `Bearer ${token}` : '';
      config.headers.common['Authorization'] = auth;
      return config;
    },
    (error) => Promise.reject(error),
  );
  

  axios2.interceptors.response.use(response => {
    return response;
 }, error => {
   if (error.response.status === 401) {
    window.location.replace('/login')
    localStorage.removeItem('access_token');
   }
   return error;
 });

export default axios2