import React, {useState, useEffect} from 'react';
 
export const getToken = () => { 
    const tokenString = localStorage.getItem('access_token'); 

    return tokenString; 
}

const useToken = () => {
 
    const [token, setToken] = useState(getToken());

    const saveToken = (accessToken) => { 
        localStorage.setItem('access_token', accessToken); 
        setToken(accessToken)
    }

    return {
        setToken: saveToken,
        token
    }
}

export default useToken;
