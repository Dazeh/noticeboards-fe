import React from 'react';

const NoticeboardListItem = ({item}) => {

     
    return (
        <div>
            <a href={`dashboard/noticeboards/${item.id}`}>{item?.name}</a>
        </div>
    );
}

export default NoticeboardListItem;
