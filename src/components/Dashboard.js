import axios from '../services/axios';
import React, {useState, useEffect} from 'react';
import useToken from '../hooks/useToken';
import NoticeboardListItem from './NoticeboardList/NoticeboardListItem';

const Dashboard = () => {

    const { token } = useToken(); 
    const [noticeboards, setNoticeboards] = useState([]);
  

    useEffect(() => {
         axios.get('/api/noticeboard/').then((response) => { 
            setNoticeboards(response.data.noticeboards);
         }).catch((e) => { 
           
         }); 
        return;
    }, []);    


    return (
        <div>
            {noticeboards.map((item) => { 
                return <NoticeboardListItem key={item.id} item={item}></NoticeboardListItem>
            })}
        </div>
    );
}

export default Dashboard;
