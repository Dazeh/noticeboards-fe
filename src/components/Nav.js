import React from 'react';
import { Link } from 'react-router-dom';

const Nav = () => {
    return (        
        <nav>
            <ul>
                <li><Link to="/noticeboards/create" className="special-button">Create New <i className="bi bi-plus"></i></Link></li>
                <li><Link to="/dashboard"><i className="bi bi-house-fill"></i>Dashboard</Link></li>
                <li><Link to="/account"><i className="bi bi-person-fill"></i>Account</Link></li>
            </ul>
            

            <ul>
                <li>
                    <Link to="/settings"><i class="bi bi-gear-fill"></i>Settings</Link>
                </li>
            </ul>
           
        </nav>
    );
}

export default Nav;
