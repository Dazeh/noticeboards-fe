import React, {useEffect, useState} from 'react'

import axios from 'axios';
import { useNavigate } from 'react-router-dom';

export default function Login({setToken}) {

  const [Username, setUsername] = useState();
  const [Password, setPassword] = useState();

  const navigate = useNavigate(); 
  
  const login = async () => { 
    axios.get('http://noticeboards.test/sanctum/csrf-cookie').then(response => {

    axios.post("http://noticeboards.test/api/login", {
      email: Username,
      password: Password,
      withCredentials: true
    })
    .then((response) => {
    
      localStorage.setItem('access_token', response.data.access_token); 

      setToken(localStorage.getItem('access_token'));

      navigate('/dashboard');
      

    }).catch((e) => { 
      console.log(e)
    });
        
});
  }

  const handleSubmit = (e) => { 
    e.preventDefault();
    login();     
  }
  return (
    <div>Login

      <form onSubmit={handleSubmit}>
        <label htmlFor="email">
          <input type="email" name="email" onChange={(e) => {setUsername(e.target.value)} } />
        </label>
        <label htmlFor="password"><input type="password" name="password" id="password" onChange={(e) => {setPassword(e.target.value)} } /></label>
        <button type="submit">
          Log In
        </button>
      </form>
    </div>
  )
}
